# Jeux Javascript

**Le projet est de faire un jeu en Javascript . Apres plusieurs idées, j'ai opté pour un quizz sur l'ecologie et le recyclage!**

**Le quizz est repartie en 6 catégories de 10 questions . Le but du jeu est de répondre correctement à toutes les questions et bien sur d'apprendre ou de revoir des pratiques sur l'ecologie et le recyclage!**

**J'ai choisi l'écologie car c'est un sujet important et d'actualité où nous avons beaucoup a apprendre !**



![](https://s3.amazonaws.com/assets.mockflow.com/app/wireframepro/company/Ceee66e9f7561c1a00df4a9c8a05fc82a/projects/M1a2941c48b81321062da0a947eb2ee191563197813902/pages/1126753c3e404b49a6a8fcfb7efbe1f6/image/1126753c3e404b49a6a8fcfb7efbe1f6.png)

# Users Stories

* En tant que joueur je veux avoir plusieurs Quizz sur differents thèmes pour parfaire mes connaissances 
* En tant que joueur je veux avoir les resultats du Quizz pour connaitre mon niveau
* En tant que recruteur je veux voir les technologies utilisées pour créer le jeu !







# Milestones	





![Capture du 2019-09-09 15-29-55](./Capture du 2019-09-09 15-29-55.png)



**Dans cette capture d'écran vous trouverez le Boards de mes milestones que j'ai  utiliser. ** **Cela m'a servi à procéder par etape à la conception de mon jeu. **





# UseCaseJeu

![UseCaseDiagramJeu](./UseCaseDiagramJeu.png)

# ClassDiagrammeQuestions

![ClassDiagramQuestion](./ClassDiagramQuestion.png)

**Exemple de class : **

![CaptureCode1](./CaptureCode1.png)









# Déroulement du jeu

**Après avoir choisi le 1er quizz à faire, on se retrouve sur cette page . Vous verrez sur l'image du dessous le code (addEventListener) qui m'a permis de remplacer la page d'accueil par le début du quiz.**



![debutQuizz](./debutQuizz.png)

![CaptureCodeQuizz](./CaptureCodeQuizz.png)





**À la fin des 10 questions, grace à un "if" j'ai pu afficher le score du quizz et quand le score est de 100%, un bouton "Go To the next quizz" apparaît pour passer au quizz suivant.**



![finQuizzWinner](./finQuizzWinner.png)

![captureCodeGood](./captureCodeGood.png)





**Et si les réponses du quizz sont fausses graces à un "else" et un "for",  j'ai pu afficher le score et toutes les réponses données par le joueur et les bonnes réponses . Vu que le score n'est pas de 100% un bouton apparaît pour recommencer le Quizz.**



![finQuizzLoose](./finQuizzLoose.png)



![CaptureCodeFalse](./CaptureCodeFalse.png)



