class Question{

    /**
     * @param {String} questionValue
     * @param {Array} answers
     * @param {String} goodAnswer
     */
    constructor(questionValue, answers, goodAnswer){
        this.question = questionValue;
        this.answers = answers;
        this.goodAnswer = goodAnswer;
        this.givedAnswer;
    }
}