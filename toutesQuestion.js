let questionsRecyclage = [new Question("Combien de temps un megot de cigarette met-il pour s'auto biodégrader? ", ["2 ans", "7 ans", "10 ans", "5 ans"], 0),
new Question("Combien de temps un chewing-gum met-il pour s'auto biodégrader?", ["1 ans", "8 ans ", "5 ans", "12 ans",], 2),
   new Question("Je jette mes cartons de pizza :", ["  Avec les cartons recyclables"," Avec les ordures ménagères"], 1),
   new Question("Dans les bacs jaunes on ne met pas :", ["Les pots de crème fraîche","Les bouteilles de lait" ,"Les bidons de sirop" ,"Le carton" ,],0),
   new Question("Où doit-on jeter les sacs plastiques et autres sacs de caisse :", ["Au recyclage","Dans la poubelle traditionnelle" ,"Dans la nature" ,"Dans la mer" ,],1),
   new Question("Pour une tonne de plastique recyclée, on économise", [" 2 Kg de pétrole brut","800 Kg de pétrole brut" ,"100 tonnes de pétrole brut" ,"1000 tonnes de pétrole brut" ,],1),
   new Question("Avec 67 bouteilles d'eau on peut créer :", ["Une couette pour 2","Une veste en polaire" ,"Un arrosoir" ,"un stylo" ,],0),
   new Question("Après mes travaux de peinture, pour me débarrasser du diluant usagé", ["Je le vide dans les toilettes","Je le vide dans le caniveau" ,"Je le mets dans un recipient hermétique et vais le porter dans un Point Propreté" ,"D la réponse D " ,],2),
   new Question("Combien de déchets chaque Français produit-il en moyenne par an ?", ["115 kg","390 kg" ," 540 kg" ,"1 tonnes" ,], 1),
   new Question("Avec le papier journal récupéré on fabrique :", ["Du papier kraft","Des boîtes à œufs" ,"De la corde" ,"Du journal" ,],1)
];


